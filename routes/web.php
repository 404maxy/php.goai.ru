<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', [App\Http\Controllers\UploaderController::class, 'index'])->name('home');

Route::get('/uploader', [App\Http\Controllers\UploaderController::class, 'index'])->name('uploader');

Route::get('/phpinfo', [App\Http\Controllers\PhpinfoController::class, 'index'])->name('phpinfo');

Route::post('/uploader/upload', [App\Http\Controllers\UploaderController::class, 'upload'])->name('upload');

Route::match(['get', 'post'], '/uploader/move', [App\Http\Controllers\UploaderController::class, 'move'])->name('move');

Route::match(['get', 'post'], '/uploader/result', [App\Http\Controllers\UploaderController::class, 'result'])->name('result');

Route::match(['get', 'post'], '/uploader/resultbyobject/{id}', [App\Http\Controllers\UploaderController::class, 'resultbyobject'])->name('resultbyobject');

Route::match(['get', 'post'], '/uploader/daemon', [App\Http\Controllers\UploaderController::class, 'daemon'])->name('daemon');

Route::match(['get', 'post'], 'search', [App\Http\Controllers\SearchController::class, 'index'])->name('search');

Route::match(['get', 'post'], 'test', [App\Http\Controllers\UploaderController::class, 'test'])->name('test');