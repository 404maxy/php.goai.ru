<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Objecta extends Model
{
    use HasFactory;
    protected $table = 'objecta';
}
