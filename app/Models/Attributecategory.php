<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attributecategory extends Model
{
    use HasFactory;
    protected $table = 'attribute_category';
}
