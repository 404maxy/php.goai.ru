<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report as Report;
use App\Models\Document as Document;
use App\Models\Documenttype as Documenttype;

class SearchController extends Controller
{
    private $uploadFolder = '/../storage/app/files';
    private $moveToFolder = '/../../storage.goai.ru/temp';
    private $pythonPath = '/../../python.goai.ru';
    private $pythonStartScript = 'http://python.goai.ru/predict';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('search.index');
    }

}
