<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report as Report;
use App\Models\Document as Document;
use App\Models\Documenttype as Documenttype;

class UploaderController extends Controller
{
    private $uploadFolder = '/../storage/app/files';
    private $moveToFolder = '/../../storage.goai.ru/temp';
    private $pythonPath = '/../../python.goai.ru';
    private $pythonStartScript = 'http://python.goai.ru/predict';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('uploader.index');
    }

    public function upload(Request $request) {

        header('Content-type:application/json;charset=utf-8');

        try {
            if (
                !isset($_FILES['file']['error']) ||
                is_array($_FILES['file']['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            switch ($_FILES['file']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            $path = $request->file('file')->store('files');

            if (!$path) {
                throw new RuntimeException('Failed to move uploaded file.');
            }

            // All good, send the response
            echo json_encode(array(
                'status' => 'ok',
                'path' => $path
            ));

        } catch (RuntimeException $e) {
            // Something went wrong, send the err message as JSON
            http_response_code(400);

            echo json_encode(array(
                'status' => 'error',
                'message' => $e->getMessage()
            ));
        }

    }

    public function move() {

        header('Content-type:application/json;charset=utf-8');
        http_response_code(400);

        $uploadFolder = $_SERVER['DOCUMENT_ROOT'].$this->uploadFolder;
        $moveToFolder = $_SERVER['DOCUMENT_ROOT'].$this->moveToFolder;

        $sourceDir = $uploadFolder;
        $destDir   = $moveToFolder;

        if (!file_exists($destDir)) {
            mkdir($destDir, 0755, true);
        }

        $dirIterator = new \RecursiveDirectoryIterator($sourceDir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator    = new \RecursiveIteratorIterator($dirIterator, \RecursiveIteratorIterator::SELF_FIRST);

        foreach ($iterator as $object) {
            $destPath = $destDir . DIRECTORY_SEPARATOR . $iterator->getSubPathName();
            if($object->isFile()) {
                copy($object, $destPath);
                unlink ( $object );
            }
        }

        $url = $this->pythonStartScript;

        // вызываем скрипт распознавания
        // $result = file_get_contents($url, false, stream_context_create(array(
        //     'http' => array(
        //         // 'method'  => 'POST',
        //         'method'  => 'GET',
        //         'header'  => 'Content-type: application/x-www-form-urlencoded',
        //         // TODO: add here authorization token, etc Basic Authtorization: ssfsdfs
        //         'content' => ''
        //     )
        // )));

        // проверь, выкинуть исключения

        echo json_encode(array(
            'status' => 'success'
        ));

    }

    public function daemon() {

        header('Content-type:application/json;charset=utf-8');
        http_response_code(400);

        $status = file_get_contents($_SERVER['DOCUMENT_ROOT'].$this->pythonPath.'/status.txt');

        if($status == 'success') { // если скрипт завешрил работу

            $sourceDir = $_SERVER['DOCUMENT_ROOT'].$this->pythonPath.'/json';

            $dirIterator = new \RecursiveDirectoryIterator($sourceDir, \RecursiveDirectoryIterator::SKIP_DOTS);
            $iterator    = new \RecursiveIteratorIterator($dirIterator, \RecursiveIteratorIterator::SELF_FIRST);

            $documents = []; // записываем в массив id документов

            foreach ($iterator as $object) { // проходим по папке с результатами
                if($object->isFile()) {
                    
                    $result = file_get_contents($object);
                    $json = json_decode($result, true); 

//                    var_dump($json);

                    // получили атрибуты
                    // Проверили на существование
                    // Записали в базу или забрали id
                    // Записали в базу отношение документ - аттрибут
                    // Получили тип документа
                    // Проверили на существование
                    // Записали в базу или забрали id
                    // Добавили id к документу
                    // Получили имя файла
                    // Добавлили имя файла к документу
                    // Получили распознаный текст
                    // Добавлили расползнаный текст к документу
                    // Получили или нет фичи [Ведомство, объект, номер документа, дата документа, counfusion matrix]
                    // Добавили фичи к документу

                    // кладем фотки - постфикс имяфайла_pictures в той же папке
                    // перемещаем файл из temp в data
                    // добавили в таблицу files
                    // добавили file_id к документу
                    // добавили статус распознования к документу

                    // записываем документ

                    // добавляем id документа в массив $documents

                    // не забываем про валидацию: кавычки, слэши, [точки, trim]



                    // foreach ($json as $v1) {
                    //     foreach ($v1 as $v2) {
                    //         if(is_array($v2)) {
                    //             foreach ($v2 as $v3) {
                    //                 echo $v3.'<br>';
                    //             }
                    //         }
                    //     }
                    // }
                }
            }

            // сериализируем $documents
            // записываем отчет
            // получаем id отчета
            // сохраняем его в сессии

            // чистим status.txt
            // чистим папку json
            

        }

        echo json_encode(array(
            'status' => $status
        ));

    }

    public function result() {

        // получаем id отчета из сессии
        $reportID = 1;
        $report = Report::where('id', $reportID)->first();

        if(!isset($report->id)) abort(404);

        $documents = Document::whereIn('id', unserialize($report->documents))->orderBy('document_type_id')->get();

        if(!$documents) abort(404);

        $documentsCount = 0;
        $documentGroups = [];

        foreach ($documents as $document) {
            $documentGroups[$document->document_type_id]['documents'][] = $document;
            $documentGroups[$document->document_type_id]['info'] = Documenttype::where('id', $document->document_type_id)->first();
            $documentsCount++;
        }


        return view('uploader.result', [
            'documentGroups' => $documentGroups,
            'documentsCount' => $documentsCount,
            'reportID' => $reportID
        ]);
    }

    public function resultbyobject($id = 1) {

        // получаем id отчета из сессии
        $reportID = $id;
        $report = Report::where('id', $reportID)->first();

        if(!isset($report->id)) abort(404);

        $documents = Document::whereIn('id', unserialize($report->documents))->orderBy('document_type_id')->get();

        if(!$documents) abort(404);

        $documentsCount = 0;
        $documentGroups = [];

        foreach ($documents as $document) {
            $documentGroups[$document->document_type_id]['documents'][] = $document;
            $documentGroups[$document->document_type_id]['info'] = Documenttype::where('id', $document->document_type_id)->first();
            $documentsCount++;
        }


        return view('uploader.result', [
            'documentGroups' => $documentGroups,
            'documentsCount' => $documentsCount,
            'reportID' => $id
        ]);
    }


    public function test() {

        $url = 'http://127.0.0.1:5000/predict';

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                // TODO: add here authorization token, etc Basic Authtorization: ssfsdfs
                'content' => ['filepath' => 'qweqwe']
            )
        )));


        echo $result;
    }

}
