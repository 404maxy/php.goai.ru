$(function(){

	var daemonTimer;

	$('#upload-files-link').click(function() {
		$('#upload-files-input').trigger('click');
	});

	$('#upload-files-form').on('submit', function(e) {

		e.preventDefault();
		
		$.ajax({
			type: 'post',
			url: '/uploader/move',
			data: null,
			type: "POST",
			dataType: null,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			processData: false,
			contentType: false,
			beforeSend: function() {}
		})
		.done(function(result) {
			var res = JSON.parse(result);
			if(res.status == 'success') {
				clearInterval(daemonTimer);
				document.location.href = '/uploader/result'
			}
			console.log(result);
		});

		$('#upload-loader').css({'display': 'flex'}).animate({'opacity': 1}, 300);
		$('#drag-and-drop-zone').animate({'opacity': 0}, 300, function(e) {
	      $(this).css({'display': 'none'});
	    });
		daemonTimer = setInterval(daemonRequest, 2000);
	    
	});


	function daemonRequest() {
		$.ajax({
			type: 'post',
			url: '/uploader/daemon',
			data: null,
			type: "POST",
			dataType: null,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			processData: false,
			contentType: false,
			beforeSend: function() {}
		})
		.done(function(result) {
			var res = JSON.parse(result);
			if(res.status == 'success') {
				clearInterval(daemonTimer);
				document.location.href = '/uploader/result'
			}
			console.log(result);
		});
	}


	$('.docpicsimg').click(function() {
		$('.popup').css('display', 'flex');
	});

	$('.x').click(function() {
		$('.popup').css('display', 'none');
	});

	$('.doc').click(function() {
		$('.docview').css('display', 'block');
	});

	$('.filter').click(function() {
		$(this).find('.inputfilter').css('display', 'block');
	});


});