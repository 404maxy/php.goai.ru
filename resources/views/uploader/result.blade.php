@extends('layouts.app')

@section('content')

<div class="containerx">
      <div class="head">
        <h2>Готово {{ $documentsCount }} документа {{ count($documentGroups) }} типов</h2>
        <div class="files-info">
          <div class="info-cercle">
            <div class="inline">точность распознавания</div>
            <div class="circle">
              <div class="circle-percent">98 %</div>
            </div>
          </div>
        </div>
      </div>
      <div class="tabs"><a href="#" class="tab current">по типам {{ count($documentGroups) }}</a><a href="/uploader/resultbyobject/{{ $reportID }}" class="tab">по объектам 2</a><a href="#" class="tab w-inline-block"><img src="/images/5f9d097b4d51d1d7fc8dce1d_search.svg" loading="lazy" width="15" alt="" class="tabicon"><div class="tabtext">поиск по документам</div></a><a href="#" class="tab tabright w-inline-block"><img src="/images/5f9d4c22a9647275edf87dbf_csv.svg" loading="lazy" alt="" class="tabicon"><div class="tabtext">выгрузить CSV</div></a></div>
      <div class="bytype">
        <div class="docs">
          @foreach($documentGroups as $documentGroup)
          <div class="type">
            <div class="typehead">{{ $documentGroup['info']->name }} <span class="num">{{ count($documentGroup['documents']) }}</span></div>
            <div class="docstype">
              @foreach($documentGroup['documents'] as $document)
              <div class="doc">
                <div class="dochead">№230013 от 12.11.2010</div>
                <div class="docinfo">
                  <div class="doctext">6 стр.</div>
                  <div class="doctext">8 атр.</div>
                  <div class="doctext">Выдавший орган</div>
                </div>
                <div class="indicator green"></div>
              </div>
              @endforeach
            </div>
          </div>
          @endforeach

        </div>
        <div class="docview" style="display: none;">
          <div class="docviewin">
            <div class="docname">Инвентаризационная карточка</div>
            <div class="docname">№230013 от 12.11.2010</div>
            <div class="docquality">
              <div class="indicator2 green"></div>
              <div class="inline">Распознано на 88 %</div><a href="#" class="linksto">уточнить</a></div>
          </div>
          <div class="docpics">
            <div class="docpicswrap"><img src="/images/5f9d2661c933a0b42013051b_image2021.png" loading="lazy" alt="" class="docpicsimg"><img src="/images/5f9d2661c933a0b42013051b_image2021.png" loading="lazy" alt="" class="docpicsimg"><img src="/images/5f9d2661c933a0b42013051b_image2021.png" loading="lazy" alt="" class="docpicsimg"></div>
          </div>
        </div>
      </div>
    </div>

@endsection