@extends('layouts.app')

@section('content')


  <div class="wideblock" id="drag-and-drop-zone">
    <div class="upload">
      <div class="uploadinfo"><img src="images/5f9d097be3ecc62d98226601_docs.svg" loading="lazy" alt="">
        <h2>Загрузите документы</h2>
        <div>Перетащите файлы в эту область<br>или <span class="link" id="upload-files-link">нажмите обзор</span></div>
        <input type="file" title='Click to add Files' id="upload-files-input" />
        <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
            <li class="text-muted text-center empty">Файлы не загружены.</li>
        </ul>
        <form action="uploader/move" method="POST" id="upload-files-form">
          {{ csrf_field() }}
          <button type="submit" name="Go" class="bigbutton">Распознать</button>
        </form>
      </div>
    </div>
  </div>

  <div class="loader" id="upload-loader">
    <div class="uploadwrap">
      <div class="uploadinfo"><img src="/images/5f9d605145546d6ee8eb99c2_Preloader_6.gif" loading="lazy" alt="" class="preloader">
        <h2>Распознавание займет некоторое время...</h2>
        <div>Страница с результатами откроется автоматически<br><span class="link">отменить</span></div>
      </div>
    </div>
  </div>

    
  <!-- File item template -->
  <script type="text/html" id="files-template">
    <li class="media">
      <div class="media-body mb-1">
        <p class="mb-2">
          <strong>%%filename%%</strong><span style="display:none;">Статус: <span class="text-muted">Ожидание</span></span>
        </p>
        <div class="progress mb-2">
          <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
            role="progressbar"
            style="width: 0%" 
            aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
          </div>
        </div>
        <hr class="mt-1 mb-1" style="display: none;" />
      </div>
    </li>
  </script>

  <!-- Debug item template -->
  <script type="text/html" id="debug-template">
    <li class="list-group-item text-%%color%%"><strong>%%date%%</strong>: %%message%%</li>
  </script>

</section>

@endsection