<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/common.css" rel="stylesheet">

    <link href="http://www.moscow-tram-collection.ru/js/uploader-1.0.2/dist/css/jquery.dm-uploader.min.css" rel="stylesheet">


</head>
<body>

    <div id="app">
        <div class="header">
            <div class="headerleft">
                <div class="logo">
                    <div class="logotext">Архи<span class="text-span">КИТ</span></div>
                </div><a href="/uploader" class="uploadbutton w-inline-block"><img src="/images/5f9d097b3d3527c8b073644f_up.svg" loading="lazy" alt="" class="buttonicon"><div class="buttontext">Загрузить</div></a><a href="/search" class="uploadbutton w-inline-block"><img src="/images/5f9d097b4d51d1d7fc8dce1d_search.svg" loading="lazy" alt="" class="buttonicon serach"><div class="buttontext">Поиск по архиву</div></a></div>
            <div class="headerright w-clearfix">
                <div class="menu">
                    <a href="#" class="menulink">модель</a><a href="#" class="menulink">аналитика</a><a href="#" class="menulink">шаблоны</a><a href="#" class="menulink">моё</a>
                    @guest
                        <a class="menulink" href="{{ route('login') }}">Войти</a>
                            @if (Route::has('register'))
                                <a class="menulink" href="{{ route('register') }}">Зарегистрироваться</a>
                            @endif
                        @else
                            <a class="menulink" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                Выйти
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form> 
                        @endguest
                </div>
                <a href="#" class="profilebutton w-inline-block"><img src="/images/5f9d097b6dc3af798929fbd6_ava.png" loading="lazy" alt=""></a>
            </div>
        </div>

        @yield('content')


        <div class="popup" style="display:none;">
            <div class="popclose w-clearfix">
                <div class="text-block">закрыть редактирование документа</div><img src="/images/5f9db5e6e13c7e3e24d5be05_cancel201.svg" loading="lazy" alt="" class="x"></div>
            <div class="popleft">
                <div><a href="#" class="owner">ОАО "Севморпуть Запад"</a><a href="#" class="objectt">Город Москва, ул. Школьная, д. 1</a>
                    <div class="line"></div>
                    <div class="docname-copy-copy">Инвентаризационная карточка</div>
                    <div class="docname-copy">№230013 от 12.11.2010</div>
                    <div class="docquality-copy">
                        <div class="indicator2 green"></div>
                        <div class="inline">Распознано на 88 %</div><a href="#" class="linksto">скачать CSV</a></div>
                </div>
                <div class="div-block">
                    <div class="docattr">
                        <div class="docattrname">Кадастровый номер</div>
                        <div class="docattrtext">2313114 1231 2</div>
                    </div>
                    <div class="docattr">
                        <div class="docattrname">Кадастровый номер</div>
                        <div class="docattrtext">2313114 1231 2</div>
                    </div>
                    <div class="docattr">
                        <div class="docattrname red">Кадастровый номер</div>
                        <div class="w-form">
                            <form id="email-form-3" name="email-form-3" data-name="Email Form 3" class="form"><input type="text" class="text-field w-input" maxlength="256" name="name" data-name="Name" id="name"><input type="submit" value="Сохранить" data-wait="Please wait..." class="submit-button w-button"></form>
                            <div class="w-form-done">
                                <div>Thank you! Your submission has been received!</div>
                            </div>
                            <div class="w-form-fail">
                                <div>Oops! Something went wrong while submitting the form.</div>
                            </div>
                        </div>
                    </div>
                    <div class="docattr">
                        <div class="docattrname red">Кадастровый номер<a href="#"><span class="linksto">уточнить</span></a><a href="#"><span class="linksto">все ок</span></a></div>
                        <div class="docattrtext">2313114 1231 2</div>
                    </div>
                    <div class="docattr">
                        <div class="docattrname">Кадастровый номер</div>
                        <div class="docattrtext">2313114 1231 2</div>
                    </div>
                    <div class="docattr">
                        <div class="docattrname">Кадастровый номер</div>
                        <div class="docattrtext">2313114 1231 2</div>
                    </div>
                    <div class="docattr">
                        <div class="docattrname">Кадастровый номер</div>
                        <div class="docattrtext">2313114 1231 2</div>
                    </div>
                    <div class="docattr">
                        <div class="docattrname">Кадастровый номер</div>
                        <div class="docattrtext">2313114 1231 2</div>
                    </div>
                    <div class="docattr">
                        <div class="docattrname">Кадастровый номер</div>
                        <div class="docattrtext">2313114 1231 2</div>
                    </div>
                </div>
            </div>
            <div class="popright"><img src="/images/5f9dae85dbdc63aa8f2a28b1_bti-docs-01.bqwhvzmeaxut.jpg" loading="lazy" srcset="/images/5f9dae85dbdc63aa8f2a28b1_bti-docs-01.bqwhvzmeaxut-p-500.jpeg 500w, /images/5f9dae85dbdc63aa8f2a28b1_bti-docs-01.bqwhvzmeaxut-p-800.jpeg 800w, /images/5f9dae85dbdc63aa8f2a28b1_bti-docs-01.bqwhvzmeaxut-p-1080.jpeg 1080w, /images/5f9dae85dbdc63aa8f2a28b1_bti-docs-01.bqwhvzmeaxut.jpg 1536w" sizes="(max-width: 479px) 100vw, (max-width: 2560px) 60vw, 1536px" alt="" class="image">
                <div class="box"></div>
            </div>
        </div>

    </div>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/uploader-1.0.2/dist/js/jquery.dm-uploader.js"></script>
    <script src="/js/uploader-1.0.2/demo/demo-ui.js"></script>
    <script src="/js/uploader-1.0.2/demo/demo-config.js"></script>
    <script src="/js/common.js"></script>

</body>
</html>
