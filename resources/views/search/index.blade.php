@extends('layouts.app')

@section('content')

    <div class="containerx">
      <div class="head">
        <h2>Поиск по архиву</h2>
      </div>
      <div class="searchwrap">
        <div class="searchresults">
          <div class="w-form">
            <form id="email-form" name="email-form" data-name="Email Form" class="searchform"><input type="text" class="search w-input" autofocus="true" maxlength="256" name="search" data-name="search" placeholder="поиск" id="search"><a href="#" class="searchbutton w-inline-block"><img src="/images/5f9d097b4d51d1d7fc8dce1d_search.svg" loading="lazy" alt="" class="searchicon"></a></form>
            <div class="w-form-done">
              <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="w-form-fail">
              <div>Oops! Something went wrong while submitting the form.</div>
            </div>
          </div>
          <div class="docssearchlist">
            <div class="textempty">Всего в архиве 45 документов. <br>Введите критерии для поиска по ним.</div>
          </div>
        </div>
        <div class="filters">
          <div class="w-form">
            <form id="email-form-2" name="email-form-2" data-name="Email Form 2">
              <div class="filtershead">Найти документы, которые содержат</div>
              <div class="filter"><label class="w-checkbox filtercheck" data-ix="filter"><input type="checkbox" id="checkbox" name="checkbox" data-name="Checkbox" class="w-checkbox-input checkbox"><span class="w-form-label" data-ix="filter">Адрес</span></label><input type="email" class="inputfilter w-input" maxlength="256" name="text" data-name="text" placeholder="содержит" id="text"></div>
              <div class="filter"><label class="w-checkbox filtercheck" data-ix="filter"><input type="checkbox" id="checkbox-5" name="checkbox-5" data-name="Checkbox 5" class="w-checkbox-input checkbox"><span class="w-form-label">Кадастровый номер</span></label><input type="email" class="inputfilter w-input" maxlength="256" name="text-5" data-name="Text 5" placeholder="содержит" id="text-5"></div>
              <div class="filter"><label class="w-checkbox filtercheck" data-ix="filter"><input type="checkbox" id="checkbox-4" name="checkbox-4" data-name="Checkbox 4" class="w-checkbox-input checkbox"><span class="w-form-label">ИНН</span></label><input type="email" class="inputfilter w-input" maxlength="256" name="text-4" data-name="Text 4" placeholder="содержит" id="text-4"></div>
              <div class="filter"><label class="w-checkbox filtercheck" data-ix="filter"><input type="checkbox" id="checkbox-3" name="checkbox-3" data-name="Checkbox 3" class="w-checkbox-input checkbox"><span class="w-form-label">Площадь</span></label><input type="email" class="inputfilter w-input" maxlength="256" name="text-3" data-name="Text 3" placeholder="содержит" id="text-3"></div>
              <div class="filter"><label class="w-checkbox filtercheck" data-ix="filter"><input type="checkbox" id="checkbox-2" name="checkbox-2" data-name="Checkbox 2" class="w-checkbox-input checkbox"><span class="w-form-label">Особые условия</span></label><input type="email" class="inputfilter w-input" maxlength="256" name="text-2" data-name="Text 2" placeholder="содержит" id="text-2"></div><input type="submit" value="Найти с атрибутами" data-wait="Please wait..." class="filtersubmit w-button"></form>
            <div class="w-form-done">
              <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="w-form-fail">
              <div>Oops! Something went wrong while submitting the form.</div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection